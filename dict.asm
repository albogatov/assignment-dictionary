%include "lib.inc"

section .text

global find_word

find_word:

	.iter:  cmp rsi, 0
		je .not
		push rdi
		push rsi
		add rsi, 8
		call string_equals
		pop rsi
		pop rdi
		cmp rax, 1
		je .found
		mov rsi, [rsi]
		jmp .iter
	.found:
		mov rax, rsi
		ret
	.not: 	mov rax, 0
		ret
		 
	
	
