section .text
%define stdout 1
%define stderr 2
global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_error
; Принимает код возврата и завершает текущий процесс
exit:
	xor rax, rax
	mov rax, 60
	syscall
	ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax, rax
.loop:
	cmp byte [rdi+rax], 0
	je .end
	inc rax
	jmp .loop
.end:
	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	mov r11, 1
	call print
	
print_error:
	mov r11, 2
	call print

print:
	xor rax, rax
	call string_length
	mov rdx, rax
	mov rax, 1
	mov rsi, rdi
	mov rdi, r11
	syscall
	ret

; Принимает код символа и выводит его в stdout
print_char:
	xor rax, rax
	push rdi
	mov rsi, rsp
	mov rdi, 1
	mov rdx, 1
	mov rax, 1
	syscall
	pop rdi
	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	xor rax, rax
	mov rax, 1
	mov rdi, 1
	mov rsi, 0xA
	mov rdx, 1
	syscall
	ret

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	xor rax, rax
	xor rcx, rcx
	dec rsp
	mov [rsp], al
	mov r10, 10
	mov rax, rdi
.iter:
	xor rdx, rdx
	div r10
	add dl, 0x30
	dec rsp
	mov [rsp], dl
	inc rcx
	test rax, rax
	jnz .iter
.print:
	mov rdi, rsp
	push rcx
	call print_string
	pop rcx
	add rsp, rcx
	inc rsp
	ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
	xor rax, rax
	cmp rdi, 0
	jge .print
	push rdi
	mov rdi, 0x2d
	call print_char
	pop rdi
	neg rdi
.print:
	call print_uint
	ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	xor rax, rax
	xor r8, r8
	xor r9, r9
.iter:
	lea r8, [rdi + rax]
	lea r9, [rsi + rax]
	mov r8b, byte[r8]
	cmp r8b, byte[r9]
	jne .neq
	cmp byte[r9], 0
	je .eq
	inc rax
	jmp .iter
.eq:
	mov rax, 1
	ret
.neq:
	mov rax, 0
	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	xor rax, rax
	push rax
	mov rax, 0
	mov rdi, 0
	mov rsi, rsp
	mov rdx, 1
	syscall
	pop rax
	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	xor rax, rax
	xor rdx, rdx
	xor r8, r8
.blank:
	push rdi
	push rdx
	push rsi
	call read_char
	pop rsi
	pop rdx
	pop rdi
	cmp al, 0x20
	je .blank
	cmp al, 0x9
	je .blank
	cmp al, 0xA
	je .blank
.loop:
	cmp rsi, rdx
	jle .fail
	lea r8, [rdi + rdx]
	mov [r8], rax
	cmp al, 0
	je .success
	inc rdx
	push rdi
	push rdx
	push rsi
	call read_char
	pop rsi
	pop rdx
	pop rdi
	cmp al, 0x20
	je .success
	cmp al, 0x9
	je .success
	cmp al, 0xA
	je .success
	jmp .loop
.fail:
	xor rdx, rdx
	mov rax, 0
	ret
.success:
	mov rax, rdi
	ret



; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor rax, rax
	xor rdx, rdx
	mov r9, 10
.iter:
	cmp byte [rdi], '0'
	jb .fin
	cmp byte [rdi], '9'
	ja .fin
	push rdx
	mul r9
	pop rdx
	add al, byte [rdi]
	sub al, 0x30
	inc rdx
	inc rdi
	jmp .iter
.fin:
	ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
	xor rax, rax
	xor rdx, rdx
	xor r11, r11
	mov r9, 10
.sign:
	cmp byte [rdi], '-'
	jne .fin
	push rsi
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	pop rsi
	inc rdi
.fin:
	push rdx
	call parse_uint
	pop r11
	add rdx, r11
	ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	xor rax, rax
	xor r10, r10
.loop:
	cmp rdx, 0
	jle .no
	lea rdi, [rdi + r10]
	lea rsi, [rsi + r10]
	mov rax, [rdi]
	mov [rsi], rax
	cmp byte [rdi], 0
	je .res
	dec rdx
	inc r10
	jmp .loop
.no:
	mov rax, 0
	xor r10, r10
	ret
.res:
	mov rax, r10
	xor r10, r10
	ret
