ASM        = nasm
ASMFLAGS   =-felf64 -g

main: main.o lib.o dict.o
	ld -o $@ $^

main.o: colon.inc words.inc main.asm lib.o dict.o
	$(ASM) $(ASMFLAGS) -o $@ $<
	
lib.o:  lib.asm
	$(ASM) $(ASMFLAGS) -o $@ $<
	
dict.o: dict.asm lib.o
	$(ASM) $(ASMFLAGS) -o $@ $<
	
