%define NEXT 0

%macro colon 2
	%%NEXT: dq NEXT
	db %1, 0
	%2: %define NEXT %%NEXT
%endmacro
