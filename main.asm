%include "lib.inc"
%include "dict.inc"
%include "words.inc"
%define ERROR_CODE 1
%define SUCCESS_CODE 0
%define BUFFER_SIZE 256

section .data
message: db "Word found", 0
error_msg: db "Incorrect input", 0
not_found_msg: db "Word not found", 0
section .text
global _start
_start:
	mov rsi, BUFFER_SIZE
	sub rsp, BUFFER_SIZE
	mov rdi, rsp
	call read_word
	cmp rdx, 0
	je .inputerr
	mov rdi, BUFFER_SIZE
	mov rsi, NEXT
	call find_word
	cmp rax, 0
	je .fail
.success:
	mov rdi, message
	call print_string
	mov rdi, SUCCESS_CODE
	call exit
.fail:
	mov rdi, not_found_msg
	call print_error
	mov rdi, ERROR_CODE
	call exit
.inputerr:
	mov rdi, error_msg
	call print_error
	mov rdi, ERROR_CODE
	call exit
